<?php
/**
 * East Coast Truss functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package East_Coast_Truss
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'east_coast_truss_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function east_coast_truss_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on East Coast Truss, use a find and replace
		 * to change 'east-coast-truss' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'east-coast-truss', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Left', 'east-coast-truss' ),
				'menu-2' => esc_html__( 'Right', 'east-coast-truss' ),
				'menu-3' => esc_html__( 'Mobile', 'east-coast-truss' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'east_coast_truss_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'east_coast_truss_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function east_coast_truss_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'east_coast_truss_content_width', 640 );
}
add_action( 'after_setup_theme', 'east_coast_truss_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function east_coast_truss_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'east-coast-truss' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'east-coast-truss' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'east_coast_truss_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function east_coast_truss_scripts() {
	wp_enqueue_style( 'east-coast-truss-bootstrap-style', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'east-coast-truss-slick-style', get_template_directory_uri() . '/libs/slick/slick.css', array(), _S_VERSION );
	wp_enqueue_style( 'east-coast-truss-slick-theme-style', get_template_directory_uri() . '/libs/slick/slick-theme.css', array(), _S_VERSION );
	wp_enqueue_style( 'east-coast-truss-lightbox-style', get_template_directory_uri() . '/libs/lightbox/ekko-lightbox.css', array(), _S_VERSION );
	wp_enqueue_style( 'east-coast-truss-main-style', get_template_directory_uri() . '/css/style.css', array(), _S_VERSION );
	wp_enqueue_style( 'east-coast-truss-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'east-coast-truss-style', 'rtl', 'replace' );

	wp_enqueue_script( 'east-coast-jquery-script', get_template_directory_uri() . '/libs/jquery/jquery-3.3.1.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'east-coast-slick-script', get_template_directory_uri() . '/libs/slick/slick.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'east-coast-bootstrap-script', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.bundle.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'east-coast-lightbox-script', get_template_directory_uri() . '/libs/lightbox/ekko-lightbox.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'east-coast-menu-script', get_template_directory_uri() . '/js/menu.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'east-coast-main-script', get_template_directory_uri() . '/js/main.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'east_coast_truss_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

