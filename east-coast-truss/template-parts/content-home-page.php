<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package East_Coast_Truss
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section id="hero-slider">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="slider-container">
						<?php

							if( have_rows('hero_slider') ):

							    while( have_rows('hero_slider') ) : the_row(); ?>

							    	<div class="slider-item">
										<div class="slider-image" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
										<div class="slider-text">
											<div class="slider-title">
												<?php the_sub_field('title'); ?> 
											</div>
											<div class="slider-subtitle">
												<?php the_sub_field('subtitle'); ?>
											</div>
										</div>
									</div>
  
							    <?php endwhile;

							endif;
						?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 offset-lg-2 px-xl-5 text-center">
					<h2>
						<?php the_field('about_title'); ?>
					</h2>
					<p>
						<?php the_field('about_description'); ?> 
					</p>
				</div>
			</div>
		</div>
	</section>
	<section id="what-we-do">
		<div class="top-curve">
			<img src="<?php echo get_template_directory_uri(); ?>/img/top-curve.svg" alt="">
		</div>
		<div class="bottom-curve">
			<img src="<?php echo get_template_directory_uri(); ?>/img/bottom-curve.svg" alt="">
		</div>
		<div class="container">
			<div class="row h-100">
				<div class="col-lg-6 my-auto pl-xl-5 order-lg-2">
					<h2 class="mb-5 text-center text-lg-left"><?php the_field('what_we_do_title'); ?></h2>
					<p class="mb-5 text-center text-lg-left">
						<?php the_field('what_we_do_description'); ?>
					</p>
					<p class="text-center text-lg-left">
						<a href="<?php the_field('what_we_do_button_url'); ?>" class="btn violet-button mb-5"><?php the_field('what_we_do_button_text'); ?></a>	
					</p>
				</div>
				<div class="col-lg-6 order-lg-1 text-center text-lg-left">
					<img src="<?php the_field('what_we_do_image'); ?>" alt="What we do" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="gallery">
		<div class="gallery-wrap">
			<?php

				if( have_rows('gallery') ):

				    while( have_rows('gallery') ) : the_row(); ?>

				    	<div class="gallery-item">
							<a href="<?php the_sub_field('item'); ?>" data-toggle="lightbox" data-gallery="photo-gallery">
								<div class="gallery-image" style="background-image: url(<?php the_sub_field('item'); ?>);"></div>
							</a>
						</div>

				    <?php endwhile;

				endif;
			?>
			
		</div>
	</section>
	<section id="why-choose-us">
		<div class="container">
			<div class="row h-100">
				<div class="col-lg-6 col-xl-5 offset-xl-1 pr-xl-5 my-auto">
					<h2 class="mb-5 text-center text-lg-left"><?php the_field('why_choose_us_title'); ?></h2>
					<div class="experience">
						<?php the_field('why_choose_us_experience'); ?>
					</div>
					<div class="team">
						<?php the_field('why_choose_us_team'); ?>
					</div>
					<div class="first-place mb-5 mb-lg-0">
						<?php the_field('why_choose_us_first_place'); ?>
					</div>
				</div>
				<div class="col-lg-6 text-center text-lg-left">
					<img src="<?php the_field('why_choose_us_image'); ?>" alt="Why Choose Us" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="opportunities">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xl-5 offset-xl-1">
					<img src="<?php the_field('opportunities_icon'); ?>" alt="" class="img-fluid mb-4">
					<p class="yellow font-weight-bold mb-4"><?php the_field('opportunities_text'); ?></p>
					<a href="<?php the_field('opportunities_button_url'); ?>" class="btn violet-button mb-5"><?php the_field('opportunities_button_text'); ?></a>
					<div class="row mb-5">
						<div class="col-5">
							<img src="<?php the_field('opportunities_map'); ?>" alt="Map" class="img-fluid">
						</div>
						<div class="col-7 my-auto">
							<h3 class="yellow mb-3">
								<?php the_field('opportunities_map_title'); ?>
							</h3>
							<p class="font-weight-bold">
								<?php the_field('opportunities_map_description'); ?>
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xl-5">
					<h2 class="mb-4">
						<?php the_field('opportunities_title'); ?>
					</h2>
					<p class="violet mb-4">
						<?php the_field('opportunities_description'); ?>  
					</p>
					<h3 class="violet mb-4">
						<?php the_field('opportunities_list_title'); ?>
					</h3>
					<ul class="ect">
						<?php

							if( have_rows('opportunities_list') ):

							    while( have_rows('opportunities_list') ) : the_row(); ?>

							    	<li><?php the_sub_field('item'); ?></li>

							    <?php endwhile;

							endif;
						?>

					</ul>
				</div>
			</div>
		</div>
	</section>
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h2 class="text-white mb-lg-5">
						<?php the_field('contact_title'); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 order-lg-2">
					<div class="form-wrap mb-5 mb-lg-0">
						<?php $form_code = get_field('contact_form'); ?>
						<?php echo do_shortcode($form_code); ?>
					</div>
				</div>
				<div class="col-lg-6 pl-xl-5 order-lg-1">
					<div class="address">
						<p><?php the_field('address'); ?></p>
						<p class="direction"><a href="<?php the_field('direction_url'); ?>" target="_blank">GET DRIVING DIRECTIONS</a></p>
					</div>
					<div class="phone">
						<div class="row">
							<div class="col-2">Phone:</div>
							<div class="col-10">
								<?php
								    $phone_number = get_field('phone');
								    $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
								?>
								<a href="tel:+1<?php echo $phone_number; ?>"><?php the_field('phone'); ?></a>
							</div>
						</div>
					</div>
					<div class="fax">
						<div class="row">
							<div class="col-2">Fax:</div>
							<div class="col-10">
								<?php
								    $phone_number = get_field('fax');
								    $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
								?>
								<a href="tel:+1<?php echo $phone_number; ?>"><?php the_field('fax'); ?></a>
							</div>
						</div>
					</div>
					<div class="email">
						<div class="row">
							<div class="col-2">Email:</div>
							<div class="col-10">
								<a href="mailto:<?php echo get_field('email');?>"><?php the_field('email'); ?></a>
							</div>
						</div>
					</div>
					<div class="copyright">
						<?php the_field('copiright'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->
