<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package East_Coast_Truss
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="row h-100">
				<div class="col-lg-2 order-lg-2">
					<div class="brand text-center">
						<?php the_custom_logo(); ?>
					</div>
					<div class="toggle-btn">
			            <span class="hamburger">
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </span>
			       	</div>
				</div>
				<div class="d-none d-lg-block col-lg-5 order-lg-1 my-auto">
					<div class="left-menu pr-xl-5">
						<div class="left-nav">
						    <div class="menu-left-menu-container">
						    	<ul id="left-menu" class="menu">
						    		<li id="menu-item-236" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-236"><a href="#about">About Us</a>
									</li>
									<li id="menu-item-237" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-237"><a href="#what-we-do">What We Do</a>
									</li>
								</ul>
							</div>						
						</div><!-- #left-nav -->
					</div>
				</div>
				<div class="d-none d-lg-block col-lg-5 order-lg-3 my-auto">
					<div class="right-menu pl-xl-5">
						<div class="right-nav">
						    <div class="menu-right-menu-container">
						    	<ul id="right-menu" class="menu">
						    		<li id="menu-item-236" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-236"><a href="#why-choose-us">Why Choose Us</a>
									</li>
									<li id="menu-item-237" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-237"><a href="#contact">Contact Us</a>
									</li>
								</ul>
							</div>						
						</div><!-- #right-nav -->
					</div>
				</div>
				<div class="main-menu d-lg-none">
					<div class="main-nav">
					    <div class="menu-main-menu-container">
					    	<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu-3',
									'menu_id'        => 'main-menu',
								)
							);
							?>
						</div>						
					</div><!-- #main-nav -->
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
